import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class SimpleCsvParser {
	List<CSVRecord> validRows = new ArrayList<CSVRecord>();
	SimpleLogging sl = new SimpleLogging();
	
	public List<CSVRecord> simpleCsvParser(String fileName) {
		Reader in;
		boolean isRecord = false;
		boolean flag = true;
		String rec = null;
		try {
			in = new FileReader(fileName);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			for (CSVRecord record : records) {
				if(isRecord) {
				int len = record.size();
				for(int i=0; i<len; i++)
				{
					rec = record.toString();
					Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
					Matcher m = p.matcher(rec);
					boolean b = m.find();
				if(record.get(i).isEmpty() || b == false) {
					flag = false;
					break;
				}
				else
				{
					flag = true;
				}
			}
				if(flag == true)
				{
					validRows.add(record);
					DirWalker.validCounter++;
				}
				else 
				{
					DirWalker.invalidCounter++;
				}
			}
				else {
					isRecord = true;
			}
		}
		
	}
		catch ( IOException e) {
			e.printStackTrace();
	}
		return validRows;
  }
}



