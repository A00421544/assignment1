import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

public class DirWalker {
	    static FileWriter fw;
		static int validCounter;
		static int invalidCounter;
		static CSVPrinter csvFilePrinter;
		List<CSVRecord> validRows = new ArrayList<CSVRecord>();
		@SuppressWarnings("resource")
		public void walk( String path ) throws IOException {
		SimpleCsvParser scp = new SimpleCsvParser();
		
		CSVFormat csvFileFormat = CSVFormat.EXCEL.withRecordSeparator("\n");
		csvFilePrinter = new CSVPrinter(fw, csvFileFormat);
		File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                //System.out.println( "Dir:" + f.getAbsoluteFile());
            }
            else {
                //System.out.println( "File:" + f.getAbsoluteFile());
                String fileName = f.getAbsoluteFile().toString();
                validRows = scp.simpleCsvParser(fileName);
                csvFilePrinter.printRecords(validRows);
             }
        }
        //csvFilePrinter.printRecords(validRows);
        
    }
		public static void main(String args[]) throws IOException
		{
			SimpleLogging.loggingFunction();
			DirWalker dw = new DirWalker();
			fw = new FileWriter("C:\\Users\\meena\\eclipse-workspace\\Assignment3\\output.csv");
			final long startTime = System.currentTimeMillis();
		    dw.walk("C:\\Users\\meena\\eclipse-workspace\\Assignment3\\Sample Data");	
		    csvFilePrinter.close();
		    final long endTime = System.currentTimeMillis();
		    Logger.getAnonymousLogger().log(Level.INFO, "Valid Rows" +validCounter);
		    Logger.getAnonymousLogger().log(Level.INFO, "Invalid Rows" +invalidCounter);
		    Logger.getAnonymousLogger().log(Level.INFO, "Total execution time: " +(endTime - startTime)+" ms");
		}
}

